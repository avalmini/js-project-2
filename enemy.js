class Enemy extends Phaser.Physics.Arcade.Sprite {
  HP;
  hasSpawned = false;
  isReady = true;
  isReloading = false;
  ammo = 0;
  pattern = 1;
  damage;
  speed;

  constructor(scene, x, y, texture) {
    super(scene, x, y, texture);
    this.HP = 250 + 250 * difficulty;
    this.damage = 10 + 5 * difficulty;
    this.speed = 50 + 10 * difficulty;
  }

  setTarget(target) {
    this.target = target;
  }

  takeDamage(damage) {
    this.HP -= damage;
  }

  sendProjectile(source, destination_x, destination_y, speed) {
    let vector = new Phaser.Math.Vector2(
      destination_x - source.x,
      destination_y - source.y
    );
    vector.normalize();
    let bullet = bullets
      .create(source.x + vector.x * 20, source.y + vector.y * 20, "pizza")
      .setScale(2 + difficulty);
    bullet.checkWorldBounds = true;
    bullet.setVelocity(vector.x * speed, vector.y * speed);
    bullet.anims.play("pizzaRotation", true);
  }

  shoot(source, destination) {
    if(this.ammo > 0 && this.pattern === 1) {
      for (let i = -5; i <= 5; i++) {
        this.sendProjectile(source, (i * 300) + destination.x, (i * 100) + destination.y, 150);
      }
      this.ammo--;
    }
    else if(this.ammo > 0 && this.pattern === 2) {
      for (let i = -5; i <= 5; i++) {
        this.sendProjectile(source, (i * 150) + destination.x, (i * 70) + destination.y, 200);
      }
      this.ammo--;
    }
    else if(this.ammo > 0 && this.pattern === 3) {
      for (let i = -5; i <= 5; i++) {
        this.sendProjectile(source, (i * 20) + destination.x, (i * 20) + destination.y, 250);
      }
      this.ammo--;
    }
    else if(this.ammo > 0 && this.pattern === 4) {
      for (let i = -5; i <= 5; i++) {
        this.sendProjectile(source, (i * 10) + destination.x, (i * 10) + destination.y, 350);
      }
      this.ammo--;
    }
  }

  reloadAmmo(){
      this.pattern = Phaser.Math.Between(1,4);
      this.ammo = 20;
      this.isReloading = false;
  }

  preUpdate(time, delta) {
    super.preUpdate(time, delta);
    if(this.alpha === 1)
      this.hasSpawned = true;
    if(this.hasSpawned) {
      if (this.x < 580) {
        this.setVelocity(this.speed, 0);
      }
      if (this.x > 700) {
        this.setVelocity(-this.speed, 0);
      }

      if (!this.target) {
        return;
      }
      const tx = this.target.x;
      const ty = this.target.y;

      const rotation = Phaser.Math.Angle.Between(this.x, this.y, tx, ty);
      this.setRotation(rotation + Phaser.Math.DegToRad(90));

      if (this.HP <= 0) {
        difficulty += 0.5;

        pizzatime.play();

        this.HP = 250 + 250 * difficulty;
        this.damage = 10 + 5 * difficulty;
        this.speed = 50 + 10 * difficulty;
        this.setScale(difficulty + 1);

        pizzaTimeText.visible = true;
      }
    }
  }

}
