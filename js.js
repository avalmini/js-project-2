var config = {
  type: Phaser.AUTO,
  width: 1280,
  height: 720,
  physics: {
    default: "arcade",
    arcade: {
      gravity: { x: 0, y: 0 },
      debug: false,
    },
  },
  scene: {
    preload: preload,
    create: create,
    update: update,
  },
};

var game = new Phaser.Game(config);
var enemies;
var player;
var enemy;
var enemyHPText;
var bullets;
var playerHPText;
var difficulty = 1;
var gameOverText;
var gameOver;
var difficultyText;
var pizzaTimeText;

function preload() {
  this.load.image("background", "assets/background.png");
  this.load.image("backgroundSpace", "assets/background2.jpg");
  this.load.spritesheet("character", "assets/character.png", {
    frameWidth: 32,
    frameHeight: 22,
  });
  this.load.spritesheet("enemy", "assets/enemy.png", {
    frameWidth: 32,
    frameHeight: 32,
  });
  this.load.spritesheet("pizza", "assets/bullet.png", {
    frameWidth: 10,
    frameHeight: 10,
  });
  this.load.audio("pizzatheme", "assets/pizzatheme.mp3");
  this.load.audio("pizzatime", "assets/pizza_time.mp3");
}

function create() {
  theme = this.sound.add("pizzatheme", {loop: true, volume: 0.05});
  pizzatime = this.sound.add("pizzatime", {loop: false, volume: 1});
  theme.play();
  this.add.image(1280 / 2, 720 / 2, "backgroundSpace").setScale(2);
  this.add.image(1280 / 2, 720 / 2, "background");
  cursors = this.input.keyboard.addKeys({
    w: Phaser.Input.Keyboard.KeyCodes.W,
    s: Phaser.Input.Keyboard.KeyCodes.S,
    a: Phaser.Input.Keyboard.KeyCodes.A,
    d: Phaser.Input.Keyboard.KeyCodes.D,
    up: Phaser.Input.Keyboard.KeyCodes.UP,
    down: Phaser.Input.Keyboard.KeyCodes.DOWN,
    left: Phaser.Input.Keyboard.KeyCodes.LEFT,
    right: Phaser.Input.Keyboard.KeyCodes.RIGHT,
    space: Phaser.Input.Keyboard.KeyCodes.SPACE,
  });

  const players = this.physics.add.group({
    classType: Player,
  });
  player = players.create(640, 450, "character").setScale(2);
  player.setCollideWorldBounds(true);
  player.setSize(22, 15, false).setOffset(5, 5);

  this.anims.create({
    key: "shoot",
    frames: this.anims.generateFrameNumbers("character", { start: 0, end: 6 }),
    frameRate: 20,
  });

  this.anims.create({
    key: "enemyShoot",
    frames: this.anims.generateFrameNumbers("enemy", { start: 0, end: 6 }),
    frameRate: 20,
  });

  this.anims.create({
    key: "pizzaRotation",
    frames: this.anims.generateFrameNumbers("pizza", { start: 0, end: 3 }),
    frameRate: 10,
    repeat: -1,
  });

  bullets = this.physics.add.group();

  enemies = this.physics.add.group({
    classType: Enemy,
  });
  enemy = enemies.create(640, 128, "enemy").setScale(2).setImmovable();
  enemy.setCollideWorldBounds(true);
  enemy.setVelocity(enemy.speed, 0);
  enemy.setTarget(player);

  enemyHPText = this.add.text(16, 16, "Enemy HP: " + enemy.HP, {
    fontSize: "32px",
    fill: "#fff",
  });
  enemyHPText.depth = 1;

  playerHPText = this.add.text(16, 16, "Player HP: " + player.HP, {
    fontSize: "32px",
    fill: "#fff",
  });
  playerHPText.depth = 1;

  difficultyText = this.add.text(16, 16, "Enemy difficulty: " + difficulty, {
    fontSize: "32px",
    fill: "#fff",
  });
  difficultyText.depth = 1;

  pizzaTimeText = this.add.text(16, 16, "Pizza time!", {
    fontSize: "64px",
    fill: "#fff",
  });
  pizzaTimeText.visible = false;
  pizzaTimeText.depth = 1;

  gameOverText = this.add.text(player.x + 640, player.y - 360, "Pizzamen is ded :(", {
    fontSize: "64px",
    fill: "#fff",
  });
  gameOverText.visible = false;
  gameOverText.depth = 1;
  enemy.alpha = 0;


  this.physics.add.collider(player, enemies);
  this.physics.add.collider(bullets, enemies, bulletHit, null, this);
  this.physics.add.collider(bullets, players, bulletHit, null, this);
  this.physics.world.setBoundsCollision(true, true, true, true);
}

var isReady = true;

function update() {
  if(enemy.alpha < 1) {
    enemy.alpha += 0.002;
    enemy.setVelocity(20, 0);
  }
  var playerDirection = new Phaser.Math.Vector2(0, 0);
  if (cursors.a.isDown) {
    playerDirection.x = -160;
  } else if (cursors.d.isDown) {
    playerDirection.x = 160;
  } else {
    playerDirection.x = 0;
  }

  if (cursors.w.isDown) {
    playerDirection.y = -160;
  } else if (cursors.s.isDown) {
    playerDirection.y = 160;
  } else {
    playerDirection.y = 0;
  }

  if (Math.abs(playerDirection.x) === Math.abs(playerDirection.y)) {
    playerDirection.x /= 1.41;
    playerDirection.y /= 1.41;
  }
  player.setVelocity(playerDirection.x, playerDirection.y);

  const rotation = Phaser.Math.Angle.Between(
    640,
    360,
    game.input.mousePointer.x,
    game.input.mousePointer.y
  );
  player.setRotation(rotation + Phaser.Math.DegToRad(90));

  if (cursors.space.isDown && player.isReady) {
    player.anims.play("shoot");
    shoot({x: 640, y:360}, game.input.mousePointer);
    player.isReady = false;
    this.time.delayedCall(player.attackSpeed - difficulty * 20, function () {
      player.isReady = true;
    });
  }

  if (enemy.isReady && enemy.HP > 0 && enemy.ammo > 0 && enemy.hasSpawned) {
    enemy.anims.play("enemyShoot");
    enemy.shoot(enemy, player);
    enemy.isReady = false;
    this.time.delayedCall(200, function () {
      enemy.isReady = true;
    });
  }
  if (!enemy.isReloading && enemy.ammo <= 0) {
    enemy.isReloading = true;
    this.time.delayedCall(1000, function () {
      enemy.reloadAmmo();
    });
  }

  bullets.children.each((enemy) => {
      if(enemy.x < 0 || enemy.x > 1280 || enemy.y < 0 || enemy.y > 720){
          enemy.destroy();
      }
  });

  if(player.HP <= 0) {
    gameOverText.visible = true;
    gameOverText.setText("Pizzamen is ded :(");
    gameOverText.setPosition(player.x - 320, player.y);
    this.time.delayedCall(2000, () => {
      gameOver = true;
    });
    if(gameOver) {
      gameOver = false;
      difficulty = 1;
      theme.stop();
      this.registry.destroy();
      this.events.off();
      this.scene.restart();
    }
  }

  enemyHPText.setText("Enemy HP: " + enemy.HP, );
  enemyHPText.setPosition(player.x + 220, player.y - 344);
  playerHPText.setText("Player HP: " + player.HP, );
  playerHPText.setPosition(player.x - 624, player.y - 344);
  difficultyText.setText("Enemy difficulty: " + difficulty, );
  difficultyText.setPosition(player.x + 220, player.y - 314);
  pizzaTimeText.setPosition(enemy.x, enemy.y);

  if(pizzaTimeText.visible){
    this.time.delayedCall(2000, function () {
      pizzaTimeText.visible = false;
    });
  }

  this.cameras.main.centerOn(player.x, player.y)
}

function bulletDestroyed(bullets, object) {
  bullets.disableBody(true, true);
}

function bulletHit(bullets, object) {
  bullets.destroy();
  object.takeDamage(10);
}

function shoot(source, destination) {
  var vector = new Phaser.Math.Vector2(
    destination.x - source.x,
    destination.y - source.y
  );
  vector.normalize();
  var bullet = bullets
    .create(player.x + vector.x * 20, player.y + vector.y * 20, "pizza")
    .setScale(2);
  bullet.setVelocity(vector.x * 400, vector.y * 400);
  bullet.anims.play("pizzaRotation", true);
}
