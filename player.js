class Player extends Phaser.Physics.Arcade.Sprite {
    HP = 500;
    isReady = true;
    attackSpeed = 200;

    constructor(scene, x, y, texture) {
        super(scene, x, y, texture);
    }

    takeDamage(damage){
        this.HP -= damage;
    }

}