class Bullet extends Phaser.Physics.Arcade.Sprite {

    constructor(scene, x, y, texture) {
        super(scene, x, y, texture);

    }

    preUpdate(time, delta) {
        if(this.x < 0 || this.x > 1280 || this.y < 0 || this.y > 720){
            this.destroy();
        }
        // this.anims.play("pizzarotation", true);
    }
}